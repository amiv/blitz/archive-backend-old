import requests
from json import loads, dumps
from flask import current_app

from werkzeug.exceptions import BadRequest

def check_auth(token, groups):
    group_met = False

    try:
        # get user id
        user_id_api = requests.get(
            current_app.config['API_URL'] + '/sessions?where={"token":"%s"}' % token, headers={'Authorization': token})
        user_id = loads(user_id_api.text)['_items'][0]['user']
        where = {
            '$and':[
                {'user': user_id},
                {'$or': [{'group': g} for g in groups]}
            ]
        }
        # request all groupmemberships of user of given groups
        api = requests.get(
            current_app.config['API_URL'] + '/groupmemberships?where=%s' % dumps(where), headers={'Authorization': token})
    except:
        raise BadRequest('AMIV-API address misconfigured or unreachable, or token invalid')
    try:
        string = api.text
        obj = loads(string)
        content = obj['_items']
        # if one or more memberships are fullfilled token is verified
        group_met = len(content) > 0

    except KeyError:
        if str(obj["_status"]) == "ERR":
            if obj["_error"]["code"] == 401:
                raise BadRequest('Invalid or expired token.')
        else:
            raise BadRequest('AMIV-API returned unknown response.')
    except:
        raise BadRequest('AMIV-API returned unknown response. Or Group membership could not be checked.')

    return group_met
