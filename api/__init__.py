from flask_restplus import Api, Resource
from flask import send_file, current_app
from .upload import api as upload
from werkzeug.exceptions import BadRequest

import os.path

api = Api(
    title='AMIV CDN',
    description='CDN where data can be uploaded authenticated the AMIV groups',
)

api.add_namespace(upload)

@api.route('/<path:directory>')
class ServeFiles(Resource):
    def get(self, directory):
        fn = os.path.join(current_app.config['BASE_DIR'], directory)
        if os.path.exists(fn):
            return send_file(fn)
        else: 
            raise BadRequest('File not found')
